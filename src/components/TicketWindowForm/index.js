import React, {Component} from 'react';
import CustomFrom from "../CustomForm";
import {connect} from 'react-redux';
import TicketWindowActions from '../../store/actions/TicketWindowActions'
import Notification from "../Notification";

class TicketWindowForm extends Component {

    static defaultProps = {
        data: {},
        currencyTypesList: [
            {id: 1, name: 'BYN'},
            {id: 2, name: 'RUB'},
            {id: 3, name: 'USD'},
            {id: 4, name: 'EUR'},
        ]
    };

    constructor(props) {
        super(props);
        this.newPay = this.newPay.bind(this);
    }

    newPay(data: Object) {
        TicketWindowActions.newPay(this.props.dispatch, data);
    }

    render() {
        console.log('ContractDetailsForm render', this.props);

        const {asyncResult, asyncOperationInProgress, currencyTypesList} = this.props;

        let model = [
            {
                title: 'Номер счета на который закидываем деньги',
                type: 'text',
                ref: 'accountNumber',
                isRequired: true,
                pattern: '^[0-9]{1,}$',
                formatString: 'Номер счета неверен!',
            },
            {
                title: 'Валюта',
                type: 'select',
                ref: 'currencyTypeId',
                isRequired: true,
                formatString: 'Валюта неверна!',
                data: currencyTypesList
            },
            {
                title: 'Сумма',
                type: 'text',
                ref: 'amount',
                isRequired: true,
                pattern: '^[0-9]{1,}$',
                formatString: 'Сумма неверена!',
            },
        ];

        return (
            <div>
                {asyncOperationInProgress
                    ? <Notification/>
                    : asyncResult === null
                        ? null
                        : asyncResult
                            ? alert('Ok!')
                            : <Notification error/>
                }
                <CustomFrom title={'Касса'} onSave={this.newPay} data={[]} model={model}/>
            </div>
        );
    }
}

const mapStateToProps = function (store) {
    return store.ticketWindowReducer;
};

export default connect(mapStateToProps)(TicketWindowForm);
