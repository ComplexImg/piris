import React, {Component} from 'react';
import CustomFrom from "../CustomForm";
import {connect} from 'react-redux';
import ClientFormActions from '../../store/actions/ClientFormActions'
import Notification from "../Notification";

class EditClientForm extends Component {

    static defaultProps = {
        data: {},
    };


    constructor(props) {
        super(props);
        this.state = {};

        this.saveData = this.saveData.bind(this);
    }


    saveData(data: Object) {

        data.id = this.props.data.id;

        console.log('saveData', data);

        ClientFormActions.saveClient(this.props.dispatch, data);
    }

    render() {
        const {cities, marital_statuses, citizenships, disabilities, error, loaded, asyncResult, asyncOperationInProgress} = this.props;

        if (cities.length === 0 && !error) {
            ClientFormActions.loadCitiesLibrary(this.props.dispatch);
        } else if (marital_statuses.length === 0 && !error) {
            ClientFormActions.loadMaritalLibrary(this.props.dispatch);
        } else if (citizenships.length === 0 && !error) {
            ClientFormActions.loadCitizenshipsLibrary(this.props.dispatch);
        } else if (disabilities.length === 0 && !error) {
            ClientFormActions.loadDisabilitiesLibrary(this.props.dispatch);
        } else if (!loaded) {
            ClientFormActions.allIsLoaded(this.props.dispatch);
        }

        if (error) {
            return <Notification error/>
        } else if (!loaded) {
            return <Notification/>
        } else {

            const model = [
                {
                    title: 'Фамилия',
                    type: 'text',
                    ref: 'middleName',
                    isRequired: true,
                    pattern: '^[A-ZА-ЯЁ][a-zа-яё]{2,20}$',
                    formatString: 'Фамилия неверна!',
                },
                {
                    title: 'Имя',
                    type: 'text',
                    ref: 'firstName',
                    isRequired: true,
                    pattern: '^[A-ZА-ЯЁ][a-zа-яё]{2,20}$',
                    formatString: 'Имя неверно!',
                },
                {
                    title: 'Отчество',
                    type: 'text',
                    ref: 'lastName',
                    isRequired: true,
                    pattern: '^[A-ZА-ЯЁ][a-zа-яё]{2,20}$',
                    formatString: 'Отчество неверно!',
                },
                {
                    title: 'Дата рождения',
                    type: 'date',
                    ref: 'birthDate',
                    isRequired: true,
                    pattern: '^(([1][9][7-9][0-9])|([2][0-9]{3}))[-][0-1][0-9][-][0-3][0-9]$',
                    formatString: 'BirthDate неверна!',
                },
                // todo Gender!!!
                {
                    title: 'Серия паспорта',
                    type: 'text',
                    ref: 'passportSeries',
                    isRequired: true,
                    pattern: '^[A-Za-z]{2}$',
                    formatString: 'Серия паспарта неверна!',
                },
                {
                    title: '№ паспорта',
                    type: 'text',
                    ref: 'passportNumber',
                    isRequired: true,
                    pattern: '^[0-9]{7}$',
                    formatString: '№ паспорта неверен!',
                },
                {
                    title: 'Кем выдан',
                    type: 'text',
                    ref: 'whoAssigned',
                    isRequired: true,
                    pattern: '^[A-ZА-ЯЁa-zа-яё]{2,20}$',
                    formatString: 'Кем выдан неверен!',
                },
                {
                    title: 'Идент. номер',
                    type: 'text',
                    ref: 'identityNumber',
                    isRequired: true,
                    pattern: '^[0-9]+$',
                    formatString: 'Идент. номер неверен!',
                },
                {
                    title: 'Место рождения',
                    type: 'text',
                    ref: 'placeBirth',
                    isRequired: true,
                    pattern: '^[A-ZА-ЯЁa-zа-яё]{2,20}$',
                    formatString: 'Место рождения неверен!',
                },
                {
                    title: 'Город факт. проживания',
                    type: 'select',
                    ref: 'currentCityId',
                    isRequired: true,
                    formatString: 'Город факт. проживания неверна!',
                    data: cities
                },
                {
                    title: 'Адрес факт.проживания',
                    type: 'text',
                    ref: 'currentAddress',
                    isRequired: true,
                    pattern: '^[A-ZА-ЯЁa-zа-яё]{2,20}$',
                    formatString: 'Адрес факт.проживания неверен!',
                },
                {
                    title: 'Телефон дом',
                    type: 'text',
                    ref: 'homePhone',
                    isRequired: true,
                    pattern: '^[0-9]{3}[-][0-9]{2}[0-9]{3}$',
                    formatString: 'Телефон дом неверен!',
                },
                {
                    title: 'Телефон моб',
                    type: 'text',
                    ref: 'mobilePhone',
                    isRequired: true,
                    pattern: '^[0-9]+$',
                    formatString: 'Телефон моб неверен!',
                },
                {
                    title: 'E-mail',
                    type: 'text',
                    ref: 'email',
                    isRequired: true,
                    pattern: '^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
                    formatString: 'E-mail неверен!',
                },
                {
                    title: 'Место работы',
                    type: 'text',
                    ref: 'workPlace',
                    isRequired: true,
                    pattern: '^[A-ZА-ЯЁa-zа-яё]{2,20}$',
                    formatString: 'Место работы неверен!',
                },
                {
                    title: 'Должность',
                    type: 'text',
                    ref: 'workPosition',
                    isRequired: true,
                    pattern: '^[A-ZА-ЯЁa-zа-яё]{2,20}$',
                    formatString: 'Должность неверен!',
                },
                {
                    title: 'Город прописки',
                    type: 'select',
                    ref: 'registrationCityId',
                    isRequired: true,
                    formatString: 'Город прописки неверна!',
                    data: cities
                },
                {
                    title: 'Адрес прописки',
                    type: 'text',
                    ref: 'registrationAddress',
                    isRequired: true,
                    pattern: '^[A-ZА-ЯЁa-zа-яё]{2,20}$',
                    formatString: 'Адрес прописки неверен!',
                },
                {
                    title: 'Семейное положение',
                    type: 'select',
                    ref: 'maritalStatusId',
                    isRequired: true,
                    formatString: 'Семейное положение неверна!',
                    data: marital_statuses
                },
                {
                    title: 'Инвалидность',
                    type: 'select',
                    ref: 'disabilityId',
                    isRequired: true,
                    formatString: 'Инвалидность неверна!',
                    data: disabilities
                },
                {
                    title: 'Пенсионер',
                    type: 'checkbox',
                    ref: 'isRetired',
                },
                {
                    title: 'Ежемесячный доход',
                    type: 'text',
                    ref: 'monthRevenue',
                    isRequired: false,
                    pattern: '^[0-9]+$',
                    formatString: 'Ежемесячный доход неверен!',
                },
                {
                    title: 'Военнообязанный',
                    type: 'checkbox',
                    ref: 'isLiableForMilitaryService',
                },
            ];

            return (
                <div>
                    {asyncOperationInProgress
                        ? <Notification/>
                        : asyncResult === null
                            ? null
                            : asyncResult
                                ? 'OK'
                                : <Notification error/>
                    }
                    <CustomFrom title={'Клиент'} onSave={this.saveData} data={this.props.data} model={model}/>
                </div>
            );

        }


    }
}

const mapStateToProps = function (store) {
    return store.formDataReducer;
};

export default connect(mapStateToProps)(EditClientForm);
