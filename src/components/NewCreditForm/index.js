import React, {Component} from 'react';
import CustomFrom from "../CustomForm";
import {connect} from 'react-redux';
import NewCreditFormActions from '../../store/actions/NewCreditFormActions'
import Notification from "../Notification";

class NewCreditForm extends Component {

    static defaultProps = {
        data: {},
    };


    constructor(props) {
        super(props);
        this.state = {
            selectedCredit: null
        };

        this.saveData = this.saveData.bind(this);
        this.onSelectCredit = this.onSelectCredit.bind(this);
    }


    saveData(data: Object) {
        NewCreditFormActions.saveCredit(this.props.dispatch, data);
    }


    onSelectCredit(id) {
        this.setState({
            selectedCredit: Number(id)
        });
    }


    render() {
        console.log('NewCreditForm render', this.props);

        const {credits, error, loaded, asyncResult, asyncOperationInProgress} = this.props;
        const {selectedCredit} = this.state;

        if (credits.length === 0 && !error) {
            NewCreditFormActions.loadCreditsLibrary(this.props.dispatch);
        } else if (!loaded) {
            NewCreditFormActions.allIsLoaded(this.props.dispatch);
        }

        if (error) {
            return <Notification error/>
        } else if (!loaded) {
            return <Notification/>
        } else {

            let model = [
                {
                    title: 'Номер паспорта',
                    type: 'text',
                    ref: 'passportNumber',
                    isRequired: true,
                    pattern: '^[0-9]{7}$',
                    formatString: 'Номер паспорта неверен!',
                },
                {
                    title: 'Сумма',
                    type: 'text',
                    ref: 'amount',
                    isRequired: true,
                    pattern: '^[0-9]{1,}$',
                    formatString: 'Сумма неверна!',
                },
                {
                    title: 'Дата начала',
                    type: 'date',
                    ref: 'startDate',
                    isRequired: true,
                    pattern: '^(([1][9][7-9][0-9])|([2][0-9]{3}))[-][0-1][0-9][-][0-3][0-9]$',
                    formatString: 'Дата начала неверна!',
                },
                {
                    title: 'Дата конца',
                    type: 'date',
                    ref: 'endDate',
                    isRequired: true,
                    pattern: '^(([1][9][7-9][0-9])|([2][0-9]{3}))[-][0-1][0-9][-][0-3][0-9]$',
                    formatString: 'Дата конца неверна!',
                },
                {
                    title: 'Тип кредита',
                    type: 'select',
                    ref: 'creditId',
                    isRequired: true,
                    formatString: 'Тип кредита неверен!',
                    data: credits,
                    onSelect: this.onSelectCredit
                },
            ];


            console.log('selectedCredit', selectedCredit);

            if (!selectedCredit) {
            } else {
                const selectedCreditObj = credits.find(credit => {
                    return credit.id === selectedCredit
                });

                const currencyTypes = selectedCreditObj.currencyTypes.map(currencyType => currencyType.currencyType);

                model.push({
                    title: 'Подробности',
                    type: 'info',
                    content: this.printCreditInfo(selectedCreditObj),
                });
                model.push({
                    title: 'Тип валюты',
                    type: 'select',
                    ref: 'currencyId',
                    isRequired: true,
                    formatString: 'Тип валюты неверен!',
                    data: currencyTypes,
                })
            }
            return (
                <div>
                    {asyncOperationInProgress
                        ? <Notification/>
                        : asyncResult === null
                            ? null
                            : asyncResult
                                ? 'OK'
                                : <Notification error/>
                    }
                    <CustomFrom title={'Кредит'} onSave={this.saveData} data={[]} model={model}/>
                </div>
            );

        }
    }

    printCreditInfo(credit) {
        return (
            <div>
                <h3><b>Название кредита: </b>{credit.name}</h3>
                <h3><b>Тип: </b>{credit.type ? 'Дифференцированный' : 'Аннуитентный'}</h3>
                <h3><b>Мин дата: </b>{credit.minExpirationTerm}</h3>
                <h3><b>Макс дата: </b>{credit.maxExpirationTerm}</h3>
                <h3><b>Описание: </b>{credit.description}</h3>
                <h3><b>Валюты: </b>{credit.currencyTypes.map((type, id) => <i
                    key={id}>{type.currencyType.name} => {type.value}%; </i>)}</h3>
            </div>
        );
    }
}

const mapStateToProps = function (store) {
    return store.newCreditFormReducer;
};

export default connect(mapStateToProps)(NewCreditForm);
