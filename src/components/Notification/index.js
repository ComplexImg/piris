import React from "react";
import img from './resize.gif';
import errorImg from './zradomer.gif';

const Notification = ({error}) => (
    <div style={{width: '100%', textAlign: 'center'}}>
        <img src={error ? errorImg : img} alt={'alt'}/>
    </div>
);


Notification.defaultValues = {
    error: false
};
export default Notification;