import React, {Component} from 'react';
import {connect} from 'react-redux';
import Notification from "../Notification";
import ClientTableActions from "../../store/actions/ClientsTableActions"
import ClientTableRow from "../ClientTableRow";
import './ClientsOverviewList.css';

class ClientsOverviewList extends Component {

    constructor(props) {
        super(props);

        this.delete = this.delete.bind(this);
    }

    delete(id){

        ClientTableActions.deleteSelectedClient(this.props.dispatch, id);
     }

    render() {
        const {clients, error, loaded} = this.props;

        if (clients.length === 0 && !error) {
            ClientTableActions.loadClientsLibrary(this.props.dispatch)
        } else if (!loaded) {
            ClientTableActions.loadClientsLibrary(this.props.dispatch);
        }

        if (error) {
            return <Notification error/>
        } else if (!loaded) {
            return <Notification/>
        } else {

            return (
                <div className='ClientsOverviewList'>
                    <table style={{width: '100%'}}>
                        <tbody>
                        {clients.map(((client, index) => <ClientTableRow onDelete={this.delete} key={index} item={client}/>))}
                        </tbody>
                    </table>
                </div>
            );
        }
    }
}

const mapStateToProps = function (store) {
    return store.ClientsTableReducer;
};

export default connect(mapStateToProps)(ClientsOverviewList);
