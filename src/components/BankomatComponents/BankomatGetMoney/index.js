import React, {Component} from 'react';
import BankomatMenu from "../BankomatMenu";
import CustomFrom from "../../CustomForm";
import BankomatActions from '../../../store/actions/BankomatActions';
import {menuItems} from '../BankomatMain';

class BankomatGetMoney extends Component {

    constructor(props) {
        super(props);
        this.onGetMoney = this.onGetMoney.bind(this);
    }

    onGetMoney(data) {
        const {userId} = this.props;
        data.accountId = userId;
        BankomatActions.getMoney(this.props.dispatch, data);
    }

    render() {
        const {response, asyncResult} = this.props;

        let text = null;

        if (asyncResult) {
            if (response.isSuccess) {

                alert('*Вы получили свои деньги*');
                BankomatActions.resetPass(this.props.dispatch, menuItems.login);

            } else {
                text = response.details;
            }

            console.log('BankomatGetMoney', response);
        }


        const model = [
            {
                title: 'Сумма денех',
                type: 'text',
                ref: 'amount',
                isRequired: true,
                pattern: '^[0-9]{1,}$',
                formatString: 'Сумма неверна',
            },
        ];

        return (
            <div>
                <BankomatMenu {...this.props}/>
                {text}
                <CustomFrom onSave={this.onGetMoney} title={'Get money'} model={model}/>
            </div>
        );
    }
}

export default BankomatGetMoney;