import React, {Component} from 'react';
import CustomFrom from "../../CustomForm";
import BankomatActions from '../../../store/actions/BankomatActions';
import {menuItems} from '../BankomatMain';

class BankomatLogin extends Component {

    constructor(props) {
        super(props);
        this.onLogin = this.onLogin.bind(this);
    }

    onLogin(data) {
        if (data.accountNumber && data.PIN) {
            BankomatActions.login(this.props.dispatch, data);
        }
    }

    render() {
        const {accountNumber, userPIN, loginCounters, response, asyncResult} = this.props;

        let text = null;

        if (asyncResult) {

            console.log('accountId', response);

            if (loginCounters[accountNumber] > 2) {
                text = 'Ваша карта заблокирована, число попыток входа превысило 3';
            } else if (response.accountId < 0) {
                text = 'Неверные данные, попробуйте еще раз';
                BankomatActions.incCounter(this.props.dispatch, accountNumber, loginCounters);
            } else {
                BankomatActions.setUser(this.props.dispatch, true, response.accountId, userPIN, accountNumber);
                BankomatActions.selectPage(this.props.dispatch, menuItems.menu);
            }
        }


        const model = [
            {
                title: 'Номер карточки',
                type: 'text',
                ref: 'accountNumber',
                isRequired: true,
                pattern: '^[0-9]{13}$',
                formatString: 'Номер карточки!',
            },
            {
                title: 'PIN',
                type: 'text',
                ref: 'PIN',
                isRequired: true,
                pattern: '^[0-9]{4}$',
                formatString: 'PIN неверен!',
            },
        ];

        const data = {
            accountNumber: accountNumber
        };

        return (
            <div>
                {text}
                <CustomFrom onSave={this.onLogin} title={'login'} model={model} data={data}/>
            </div>
        );
    }
}

export default BankomatLogin;