import React, {Component} from 'react';
import {connect} from 'react-redux';

import './styles.css';

import Notification from "../../Notification";

import BankomatLogin from "../BankomatLogin";
import BankomatMenu from "../BankomatMenu";
import BankomatGetInfo from "../BankomatGetInfo";
import BankomatGetMoney from "../BankomatGetMoney";
import BankomatNewPayment from "../BankomatNewPayment";


export const menuItems = {
    login: 0,
    getInfo: 1,
    getMoney: 2,
    newPayment: 3,
    menu: 4,
};

class BankomatMain extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedCredit: null
        };

        this.getSelectedWindow = this.getSelectedWindow.bind(this);
    }


    getSelectedWindow() {
        const windowsList = {
            0: props => <BankomatLogin {...props}/>,
            1: props => <BankomatGetInfo {...props}/>,
            2: props => <BankomatGetMoney {...props}/>,
            3: props => <BankomatNewPayment {...props}/>,
            4: props => <BankomatMenu {...props} fullMenu={true}/>,
        };

        const {
            currentWindow,
        } = this.props;

        const selectedWindow = windowsList[Number(currentWindow)];

        return (
            selectedWindow(this.props)
        );
    }

    render() {
        console.log('BankomatMain', this.props);

        const {asyncOperationInProgress, asyncResult} = this.props;

        return (
            <div className={'BankomatMain'}>
                {asyncOperationInProgress
                    ? <Notification/>
                    : asyncResult === null
                        ? this.getSelectedWindow()
                        : asyncResult
                            ? this.getSelectedWindow()
                            : <Notification error/>
                }
            </div>
        );
    }

}

const mapStateToProps = function (store) {
    return store.bankomatReducer;
};

export default connect(mapStateToProps)(BankomatMain);
