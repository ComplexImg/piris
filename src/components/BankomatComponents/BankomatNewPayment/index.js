import React, {Component} from 'react';
import BankomatMenu from "../BankomatMenu";
import CustomFrom from "../../CustomForm";
import BankomatActions from "../../../store/actions/BankomatActions";
import {menuItems} from "../BankomatMain";

class BankomatNewPayment extends Component {

    constructor(props) {
        super(props);
        this.onSendMoney = this.onSendMoney.bind(this);
    }

    onSendMoney(data) {
        if (data.amount && data.accountNumberTo) {
            const {userId} = this.props;

            data.accountId = userId;

            BankomatActions.sendMoney(this.props.dispatch, data);
        }
    }

    render() {
        const {response, asyncResult} = this.props;

        let text = null;

        if (asyncResult) {
            if (response.isSuccess) {

                alert('*Ваши деньши отправлены*');
                BankomatActions.resetPass(this.props.dispatch, menuItems.login);

            } else {
                text = response.details;
            }

            console.log('BankomatNewPayment', response);
        }

        const model = [
            {
                title: 'Кому, номер карты',
                type: 'text',
                ref: 'accountNumberTo',
                isRequired: true,
                pattern: '^[0-9]{13}$',
                formatString: 'номер карты неверен!',
            },
            {
                title: 'Сумма денех',
                type: 'text',
                ref: 'amount',
                isRequired: true,
                pattern: '^[0-9]{1,}$',
                formatString: 'Сумма неверна',
            }
        ];

        return (
            <div>
                <BankomatMenu {...this.props}/>
                {text}
                <CustomFrom onSave={this.onSendMoney} title={'Send money'} model={model}/>
            </div>
        );
    }
}

export default BankomatNewPayment;