import React, {Component} from 'react';
import BankomatMenu from "../BankomatMenu";
import BankomatActions from '../../../store/actions/BankomatActions';

class BankomatGetInfo extends Component {

    render() {
        const {userId, response, asyncResult} = this.props;

        if (!asyncResult) {
            BankomatActions.getUserInfo(this.props.dispatch, {
                accountId: userId
            })
        } else {
            // todo add in page view
            console.log('BankomatGetInfo', response);
        }


        const currencies = {
            1: 'BYN',
            2: 'RUB',
            3: 'USD',
            4: 'EUR'
        };

        return (
            <div>
                <BankomatMenu {...this.props}/>
                {asyncResult ? (
                    <div>
                        <h3><b>Номер счета: </b>{response.accountNumber}</h3>
                        <h3><b>Валюта: </b>{currencies[response.currencyTypeId]}</h3>
                        <h3><b>Остаток: </b>{response.summaryAmount}</h3>
                    </div>
                ) : null}

            </div>
        );
    }
}

export default BankomatGetInfo;