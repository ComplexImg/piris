import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {menuItems} from '../BankomatMain';

import BankomatActions from "../../../store/actions/BankomatActions";


class BankomatMenu extends Component {

    static propTypes = {
        fullMenu: PropTypes.bool
    };

    static defaultProps = {
        fullMenu: false
    };

    constructor(props) {
        super(props);
        this.onSelectPage = this.onSelectPage.bind(this);
        this.getMyCard = this.getMyCard.bind(this);
    }

    onSelectPage(pageId) {
        BankomatActions.selectPage(this.props.dispatch, pageId);
    }

    getMyCard() {
        BankomatActions.getCard(this.props.dispatch, menuItems.login);
    }

    render() {
        const {fullMenu} = this.props;

        if (fullMenu) {
            return (
                <div>
                    <button onClick={() => this.onSelectPage(menuItems.getInfo)}>Get info</button>
                    <button onClick={() => this.onSelectPage(menuItems.getMoney)}>Get money</button>
                    <button onClick={() => this.onSelectPage(menuItems.newPayment)}>New Payment</button>
                    <button onClick={this.getMyCard}>Get my card</button>
                </div>
            );
        } else {
            return (
                <div>
                    <button onClick={() => this.onSelectPage(menuItems.menu)}>Menu</button>
                    <button onClick={this.getMyCard}>Get my card</button>
                </div>
            );
        }


    }
}

export default BankomatMenu;