import React, {Component} from 'react';
import CustomFrom from "../CustomForm";
import {connect} from 'react-redux';
import NewContractFormActions from '../../store/actions/NewContractFormActions'
import Notification from "../Notification";

class NewContractForm extends Component {

    static defaultProps = {
        data: {},
    };


    constructor(props) {
        super(props);
        this.state = {
            selectedDeposite: null
        };

        this.saveData = this.saveData.bind(this);
        this.onSelectDeposit = this.onSelectDeposit.bind(this);
    }


    saveData(data: Object) {

        console.log('saveData', data);

        NewContractFormActions.saveContract(this.props.dispatch, data);
    }


    onSelectDeposit(id) {
        this.setState({
            selectedDeposite: Number(id)
        });
    }


    render() {

        console.log('NewContractForm render', this.props);

        const {deposits, error, loaded, asyncResult, asyncOperationInProgress} = this.props;
        const {selectedDeposite} = this.state;

        if (deposits.length === 0 && !error) {
            NewContractFormActions.loadDipositsLibrary(this.props.dispatch);
        } else if (!loaded) {
            NewContractFormActions.allIsLoaded(this.props.dispatch);
        }

        if (error) {
            return <Notification error/>
        } else if (!loaded) {
            return <Notification/>
        } else {

            let model = [
                {
                    title: 'Номер паспорта',
                    type: 'text',
                    ref: 'passportNumber',
                    isRequired: true,
                    pattern: '^[0-9]{7}$',
                    formatString: 'Номер паспорта неверен!',
                },
                {
                    title: 'Сумма',
                    type: 'text',
                    ref: 'amount',
                    isRequired: true,
                    pattern: '^[0-9]{1,}$',
                    formatString: 'Сумма неверна!',
                },
                {
                    title: 'Дата начала',
                    type: 'date',
                    ref: 'startDate',
                    isRequired: true,
                    pattern: '^(([1][9][7-9][0-9])|([2][0-9]{3}))[-][0-1][0-9][-][0-3][0-9]$',
                    formatString: 'Дата начала неверна!',
                },
                {
                    title: 'Дата конца',
                    type: 'date',
                    ref: 'endDate',
                    isRequired: true,
                    pattern: '^(([1][9][7-9][0-9])|([2][0-9]{3}))[-][0-1][0-9][-][0-3][0-9]$',
                    formatString: 'Дата конца неверна!',
                },
                {
                    title: 'Тип депозита',
                    type: 'select',
                    ref: 'depositId',
                    isRequired: true,
                    formatString: 'Тип депозита неверен!',
                    data: deposits,
                    onSelect: this.onSelectDeposit
                },
            ];


            if (!selectedDeposite) {
            } else {
                const selectedDepositeObj = deposits.find(deposit => {
                    return deposit.id === selectedDeposite
                });

                const currencyTypes = selectedDepositeObj.currencyTypes.map(currencyType => currencyType.currencyType);

                model.push({
                    title: 'Подробности',
                    type: 'info',
                    content: this.printDepositeInfo(selectedDepositeObj),
                });
                model.push({
                    title: 'Тип валюты',
                    type: 'select',
                    ref: 'currencyId',
                    isRequired: true,
                    formatString: 'Тип валюты неверен!',
                    data: currencyTypes,
                })
            }
            return (
                <div>
                    {asyncOperationInProgress
                        ? <Notification/>
                        : asyncResult === null
                            ? null
                            : asyncResult
                                ? 'OK'
                                : <Notification error/>
                    }
                    <CustomFrom title={'Контракт'} onSave={this.saveData} data={[]} model={model}/>
                </div>
            );

        }
    }

    printDepositeInfo(deposit) {
        // todo deposit.type set!!!
        return (
            <div>
                <h3><b>Название депозита: </b>{deposit.name}</h3>
                <h3><b>Тип: </b>{deposit.type ? 'Не отзывной' : 'Отзывной'}</h3>
                <h3><b>Мин дата: </b>{deposit.minExpirationTerm}</h3>
                <h3><b>Макс дата: </b>{deposit.maxExpirationTerm}</h3>
                <h3><b>Описание: </b>{deposit.description}</h3>
                <h3><b>Валюты: </b>{deposit.currencyTypes.map((type, id) => <i key={id}>{type.currencyType.name} => {type.value}%;  </i> )}</h3>
            </div>
        );
    }
}

const mapStateToProps = function (store) {
    return store.newContractFormReducer;
};

export default connect(mapStateToProps)(NewContractForm);
