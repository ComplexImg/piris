import React, {Component} from 'react';
import {connect} from 'react-redux';
import ContractDetailsActions from '../../store/actions/ContractDetailsActions'
import Notification from "../Notification";

class CloseBankDayForm extends Component {

    static defaultProps = {
        data: {},
    };

    constructor(props) {
        super(props);
        this.closeDay = this.closeDay.bind(this);
    }

    closeDay() {
        ContractDetailsActions.closeDayDetails(this.props.dispatch);
    }

    render() {
        console.log('CloseBankDayForm render', this.props);

        const {details, asyncResult, asyncOperationInProgress} = this.props;

        return (
            <div className={'CustomFrom'}>
                {asyncOperationInProgress
                    ? <Notification/>
                    : asyncResult === null
                        ? null
                        : asyncResult
                            ? alert('Закрыли')
                            : <Notification error/>
                }
                <button onClick={this.closeDay}>Close</button>
                {this.printDetails(details)}
            </div>
        );
    }

    printDetails(details){
        if(details){
            return ('HIII')
        }
    }
}

const mapStateToProps = function (store) {
    return store.contractDetailsReducer;
};

export default connect(mapStateToProps)(CloseBankDayForm);
