import React from "react";
import img from './resize.gif';

const Loading = () => (
    <div style={{width: '100%', textAlign: 'center'}}>
        <img src={img}/>
    </div>
);

export default Loading;