import React, {Component} from 'react';
import CustomFrom from "../CustomForm";
import {connect} from 'react-redux';
import ContractDetailsActions from '../../store/actions/ContractDetailsActions'
import Notification from "../Notification";

class ContractDetailsForm extends Component {

    static defaultProps = {
        data: {},
        currecyTypesById: {
            '1': 'BYN',
            '2': 'RUB',
            '3': 'USD',
            '4': 'EUR',
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            selectedDeposite: null
        };

        this.getContractInfo = this.getContractInfo.bind(this);
        this.printDetails = this.printDetails.bind(this);
    }

    getContractInfo(data: Object) {
        const contractId = Number(data.number);
        if (contractId) {
            ContractDetailsActions.loadContractDetails(this.props.dispatch, contractId);
        }
    }

    render() {
        console.log('ContractDetailsForm render', this.props);

        const {details, asyncResult, asyncOperationInProgress} = this.props;

        let model = [
            {
                title: 'Номер счета о котором хотите получить информацию',
                type: 'text',
                ref: 'number',
                isRequired: true,
                pattern: '^[0-9]{1,}$',
                formatString: 'Номер счета неверен!',
            },
        ];

        return (
            <div>
                {asyncOperationInProgress
                    ? <Notification/>
                    : asyncResult === null
                        ? null
                        : asyncResult
                            ? 'ok'
                            : <Notification error/>
                }
                <CustomFrom title={'Информация о контракте'} onSave={this.getContractInfo} data={[]} model={model}/>
                {this.printDetails(details)}
            </div>
        );
    }

    printDetails(details) {
        if (details) {
            return (
                <div className={'CustomFrom'}>
                    <h3><b>Номер счета: </b>{details.accountNumber}</h3>
                    <h3><b>Дата создания: </b>{details.creationDate}</h3>
                    <h3><b>Тип валюты: </b>{this.props.currecyTypesById[details.currencyTypeId]}</h3>
                    {details.credit === 0 ?
                        (<h3><b>Дебит: </b>{details.debit}</h3>) :
                        (<h3><b>Кредит: </b>{details.credit}</h3>)
                    }
                    <h3><b>Сальдо: </b>{details.saldo}</h3>
                    <h3><b>Закрыт: </b>{details.isClosed ? 'да' : 'нет'}</h3>
                    <h3><b>clientId: </b>{details.clientId}</h3>
                    <h3><b>contractId: </b>{details.contractId}</h3>
                    <h3><b>accountName: </b>{details.accountName}</h3>
                </div>
            )
        }
    }
}

const mapStateToProps = function (store) {
    return store.contractDetailsReducer;
};

export default connect(mapStateToProps)(ContractDetailsForm);
