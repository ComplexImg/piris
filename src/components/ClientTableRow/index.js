import React, {Component} from 'react';
import EditClientForm from "../../components/EditClientForm";

class ClientTableRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        };

        this.open = this.open.bind(this);
        this.delete = this.delete.bind(this);
    }

    open() {
        this.setState({
            open: !this.state.open
        });
    }

    delete() {

        this.props.onDelete(this.props.item.id)
    }

    render() {
        const {open} = this.state;
        const {item} = this.props;

        console.log(item);


        if(open){
            return (
                <tr>
                    <td colSpan="4">
                        <button onClick={this.open}>Close</button>
                        <button onClick={this.delete}>Delete</button>
                        <EditClientForm data={item}/>
                    </td>
                </tr>
            );
        }else{
            return (
                <tr>
                    <td>{item.id}</td>
                    <td>{item.firstName}</td>
                    <td>{item.middleName}</td>
                    <td>
                        <button onClick={this.open}>Edit</button>
                    </td>
                </tr>
            );
        }


    }

}

export default ClientTableRow;