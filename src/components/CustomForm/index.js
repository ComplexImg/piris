import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ValidatedField from "../FormComponents/ValidatedField";
import CustomCheckbox from "../FormComponents/CustomCheckbox";
import CustomDateField from "../FormComponents/CustomDateField";
import CustomSelectBox from "../FormComponents/CustomSelectBox";
import "./CustomFrom.css";

class CustomFrom extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        model: PropTypes.arrayOf(
            PropTypes.shape({
                type: PropTypes.string,
                title: PropTypes.string,
                ref: PropTypes.string,
                isRequired: PropTypes.bool,
                pattern: PropTypes.string,
                formatString: PropTypes.string,
                content: PropTypes.node
            })
        ).isRequired,
    };

    static defaultProps = {
        data: {}
    };


    constructor(props) {
        super(props);
        this.state = {};

        this.render = this.render.bind(this);
        this.subm = this.subm.bind(this);
    }

    subm(e) {
        e.preventDefault();
        const {model} = this.props;


        let data = {};

        let isAllRight = true;

        model.forEach((field) => {
            if(field.type !== 'info'){
                data[field.ref] = this.refs[field.ref].getData().value;

                if(isAllRight){
                    isAllRight = this.refs[field.ref].getData().isCorrect;
                }
            }
        });

        console.log('------------------');
        console.log("data", data);
        console.log('isAllRight', isAllRight);

        if (isAllRight) {
            this.props.onSave(data);
        }else{
            alert("Проверьте поля!");
        }


    }

    render() {
        const {model, title, data} = this.props;

        return (
            <form onSubmit={this.subm} className={'CustomFrom'}>
                <h1>{title}</h1>

                {model.map((field, index) => {
                    switch (field.type) {
                        case 'info':
                            return (
                                <div key={index} className={'CustomFromBlock'}>
                                    <label>{field.title}</label>
                                    <div>{field.content}</div>
                                </div>
                            );
                        case 'text':
                            return (
                                <div key={index} className={'CustomFromBlock'}>
                                    <label>{field.title}<span
                                        style={{color: 'red'}}>{field.isRequired ? '*' : ''}</span>:</label>
                                    <ValidatedField
                                        ref={field.ref}
                                        defaultValue={data[field.ref]}
                                        formatString={field.formatString}
                                        pattern={field.pattern}
                                        isRequired={field.isRequired}/>
                                </div>
                            );
                        case 'checkbox':
                            return (
                                <div key={index} className={'CustomFromBlock'}>
                                    <label>{field.title}<span
                                        style={{color: 'red'}}>{field.isRequired ? '*' : ''}</span>:</label>
                                    <CustomCheckbox
                                        ref={field.ref}
                                        defaultValue={data[field.ref]}
                                        isRequired={field.isRequired}/>
                                </div>
                            );
                        case 'date':
                            return (
                                <div key={index} className={'CustomFromBlock'}>
                                    <label>{field.title}<span
                                        style={{color: 'red'}}>{field.isRequired ? '*' : ''}</span>:</label>
                                    <CustomDateField
                                        ref={field.ref}
                                        defaultValue={data[field.ref]}
                                        formatString={field.formatString}
                                        pattern={field.pattern}
                                        isRequired={field.isRequired}/>
                                </div>
                            );
                        case 'select':
                            return (
                                <div key={index} className={'CustomFromBlock'}>
                                    <label>{field.title}<span
                                        style={{color: 'red'}}>{field.isRequired ? '*' : ''}</span>:</label>
                                    <CustomSelectBox
                                        ref={field.ref}
                                        selectData={field.data}
                                        defaultValue={data[field.ref]}
                                        formatString={field.formatString}
                                        pattern={field.pattern}
                                        isRequired={field.isRequired}
                                        onSelect={field.onSelect}
                                    />
                                </div>
                            );
                        default:
                            return null;
                    }
                })}
                <input type={'submit'}/>
            </form>
        );
    }

}

export default CustomFrom;