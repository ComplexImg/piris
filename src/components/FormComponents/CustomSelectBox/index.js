import React, {Component} from 'react';
import PropTypes from 'prop-types';

class CustomSelectBox extends Component {

    static propTypes = {
        selectData: PropTypes.arrayOf(
            PropTypes.shape({
                Id: PropTypes.number,
                Name: PropTypes.string,
            })
        ).isRequired,
        defaultValue: PropTypes.number,
        isRequired: PropTypes.bool,
        onSelect: PropTypes.func,
    };

    constructor(props) {
        super(props);

        this.state = {
            value: this.props.defaultValue,
            isCorrect: true
        };

        this.render = this.render.bind(this);
        this.selectType = this.selectType.bind(this);
        this.getData = this.getData.bind(this);
    }

    getData() {
        const {value} = this.state;
        const {isRequired} = this.props;

        this.setState({
            isCorrect: isRequired ? Boolean(Number(value)) : true,
        });


        return {
            value: value,
            isCorrect: isRequired ? Boolean(Number(value)) : true,
        }
    }


    selectType(event) {
        if (this.props.onSelect) {
            this.props.onSelect(event.target.value);
        }

        this.setState({
            isCorrect: this.props.isRequired ? Boolean(Number(event.target.value)) : true,
            value: event.target.value
        });
    }

    render() {
        const {selectData, formatString} = this.props;
        const {value, isCorrect} = this.state;

        if (isCorrect) {
            return (
                <div className={'ValidatedField'}>
                    <select onChange={this.selectType} className="CustomSelectBox" defaultValue={value}>
                        <option value={0} key={-1}>No selected</option>
                        {selectData.map(function (obj, idx) {
                            return <option value={Number(obj.id)} key={idx}>{obj.name}</option>;
                        })}
                    </select>
                </div>
            );

        } else {
            return (
                <div className={'ValidatedField isNotCorrect'}>
                    <select onChange={this.selectType} className="CustomSelectBox" defaultValue={value}>
                        <option value={0} key={-1}>No selected</option>
                        {selectData.map(function (obj, idx) {
                            return <option value={obj.id} key={idx}>{obj.name}</option>;
                        })}
                    </select>
                    <p>{formatString}</p>
                </div>
            );

        }
    }

}


export default CustomSelectBox