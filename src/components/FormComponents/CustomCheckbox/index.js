import React, {Component} from 'react';
import PropTypes from 'prop-types';

class CustomCheckbox extends Component {

    static propTypes = {
        defaultValue: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.getData = this.getData.bind(this);
    }


    getData() {
        return {
            value: this.refs.CustomCheckboxRef.checked,
            isCorrect: true,
        }
    }


    render() {
        const {defaultValue} = this.props;
        return (
            <div className={'CustomCheckbox'}>
                <input ref={'CustomCheckboxRef'} type={'checkbox'} checked={defaultValue}/>
            </div>
        );
    }
}

export default CustomCheckbox;