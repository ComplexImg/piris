import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './ValidatedField.css';

class ValidatedField extends Component {

    static propTypes = {
        pattern: PropTypes.string,
        defaultValue: PropTypes.string,
        formatString: PropTypes.string,
        isRequired: PropTypes.bool,
    };


    constructor(props) {
        super(props);

        this.state = {
            value: this.props.defaultValue,
            isCorrect: true
            // isCorrect: this.checkField(this.props.defaultValue)
        };

        this.editField = this.editField.bind(this);
        this.checkField = this.checkField.bind(this);
        this.getData = this.getData.bind(this);
    }


    getData(){
        this.setState({
            isCorrect: this.checkField(this.state.value)
        });

        return {
            value: this.state.value,
            isCorrect: this.state.isCorrect,
        }
    }

    checkField(field: string) {
        field = (typeof field) === "undefined" ? '' : String(field);

        const {isRequired, pattern} = this.props;
        const regex = new RegExp(pattern);

        return field.length === 0
            ? !isRequired
            : regex.test(field);
    }


    editField(e) {
        const value = e.target.value;
        this.setState({
            value: value,
            isCorrect: this.checkField(value)
        });
    }


    render() {
        const {value, isCorrect} = this.state;
        const {formatString} = this.props;

        if (isCorrect) {
            return (
                <div className={'ValidatedField'}>
                    <input onChange={this.editField} type={'text'} defaultValue={value}/>
                </div>
            );
        } else {
            return (
                <div className={'ValidatedField isNotCorrect'}>
                    <input onChange={this.editField} type={'text'} defaultValue={value}/>
                    <p>{formatString}</p>
                </div>
            );
        }
    }
}

export default ValidatedField;