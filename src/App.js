import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import Init from "./containers/Init";
import ClientsTable from "./containers/ClientsTable";
import NewContract from "./containers/NewContract";
import Store from "./store/Store";
import { Provider } from 'react-redux';
import ContractDetails from "./containers/ContractDetails";
import CloseBankDay from "./containers/CloseBankDay";
import TicketWindow from "./containers/TicketWindow";
import NewCredit from "./containers/NewCredit";
import BankomatWindow from "./containers/BankomatWindow";



const Inittt = () => (
    <div>
        <Init/>
    </div>
);


const List = () => (
    <div>
        <Provider store={Store}><ClientsTable/></Provider>
    </div>
);


class App extends Component {
    render() {
        return (
            <div className="App">
                <Router>
                    <div>
                        <ul>
                            <li>
                                <Link to="/">List</Link>
                            </li>
                            <li>
                                <Link to="/edit">Client form</Link>
                            </li>
                            <li>
                                <Link to="/new_contract">New contract</Link>
                            </li>
                            <li>
                                <Link to="/contract_details">Contract details</Link>
                            </li>
                            <li>
                                <Link to="/close_day">Close bank day</Link>
                            </li>
                            <li>
                                <Link to="/window">Ticket window</Link>
                            </li>
                            <li>
                                <Link to="/new_credit">New credit</Link>
                            </li>
                            <li>
                                <Link to="/bankomat">Bankomat</Link>
                            </li>
                        </ul>
                        <hr/>
                        <Route exact path="/" component={List}/>
                        <Route exact path="/edit" component={Inittt}/>
                        <Route exact path="/new_contract" component={NewContract}/>
                        <Route exact path="/contract_details" component={ContractDetails}/>
                        <Route exact path="/close_day" component={CloseBankDay}/>
                        <Route exact path="/window" component={TicketWindow}/>
                        <Route exact path="/new_credit" component={NewCredit}/>
                        <Route exact path="/bankomat" component={BankomatWindow}/>

                    </div>
                </Router>

            </div>
        );
    }
}

export default App;
