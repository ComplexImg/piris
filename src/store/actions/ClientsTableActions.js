import { AbstractAction } from './abstractAction';
import { methods } from "../reducers/ClientsTableReducer";

const requestLoadSuccess = (data) => {
    return {type: methods.REQUESTED_CLIENTS_SUCCESS, data: data}
};
const requestLoadFailure = () => {
    return {type: methods.REQUESTED_CLIENTS_FAILURE}
};


const requestEventsAsync = () => {
    return {type: methods.REQUESTED_CLIENTS_ASYNC}
};
const requestEventsErrorAsync = () => {
    return {type: methods.REQUESTED_CLIENTS_ASYNC_FAILED}
};

const ClientTableActions = {

    deleteSelectedClient(dispatch: Function, id: number) {

        dispatch(requestEventsAsync());

        const url = AbstractAction.url + 'clients/' + id;

        return AbstractAction.ajaxToServer(
            url,
            null,
            data => ClientTableActions.loadClientsLibrary(dispatch),
            err => dispatch(requestEventsErrorAsync()),
            "DELETE"
        );
    },

    loadClientsLibrary(dispatch: Function) {
        const url = AbstractAction.url + 'clients';

        return AbstractAction.ajaxGetToServer(
            url,
            data => dispatch(requestLoadSuccess(data)),
            err => dispatch(requestLoadFailure())
        )
    },

};


export default ClientTableActions;