import {AbstractAction} from './abstractAction';
import {methods} from "../reducers/NewCreditFormReducer";

const requestEventsError = () => {
    return {type: methods.REQUESTED_FAILED}
};
const requestCreditsSuccess = (data) => {
    return {type: methods.REQUESTED_SET_CREDIT, data: data}
};

const requestEventsSuccesed = () => {
    return {type: methods.REQUESTED_SUCCEEDED}
};

const requestAsyncStart = () => {
    return {type: methods.REQUESTED_START_ASYNC}
};
const requestAsyncSuccess = () => {
    return {type: methods.REQUESTED_SUCCESS_ASYNC}
};
const requestAsyncFailure = (err) => {
    return {type: methods.REQUESTED_FAILURE_ASYNC}
};


const NewCreditFormActions = {

    allIsLoaded(dispatch: Function) {
        dispatch(requestEventsSuccesed())
    },

    loadCreditsLibrary(dispatch: Function) {
        const url = AbstractAction.url + 'credits';

        return AbstractAction.ajaxGetToServer(
            url,
            data => dispatch(requestCreditsSuccess(data)),
            err => dispatch(requestEventsError())
        )
    },


    saveCredit(dispatch: Function, data: Object) {

        requestAsyncStart();

        const url = AbstractAction.url + 'credit';

        data.amount = Number(data.amount);
        data.currencyId = Number(data.currencyId);
        data.creditId = Number(data.creditId);
        data.passportNumber = Number(data.passportNumber);

        console.log('saveCredit', data);

        return AbstractAction.ajaxToServer(
            url,
            data,
            data => dispatch(requestAsyncSuccess()),
            err => dispatch(requestAsyncFailure())
        );
    },
};


export default NewCreditFormActions;