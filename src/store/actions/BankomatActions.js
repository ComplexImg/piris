import {AbstractAction} from './abstractAction';
import {methods} from "../reducers/BankomatReducer";

const requestAsyncStart = () => {
    return {type: methods.REQUESTED_START_ASYNC}
};
const requestAsyncSuccess = (data) => {
    console.log('requestAsyncSuccess', data);

    return {type: methods.REQUESTED_SUCCESS_ASYNC, data: data}
};
const requestAsyncFailure = (err) => {
    console.error(err);
    return {type: methods.REQUESTED_FAILURE_ASYNC}
};
const setUser = (userCorrect, userId, userPIN, accountNumber) => {
    return {type: methods.REQUESTED_SET_USER, userCorrect, userId, userPIN, accountNumber}
};

const setCounter = (loginCounters) => {
    return {type: methods.REQUESTED_SET_COUNTER, loginCounters}
};

const setPage = (currentWindow) => {
    return {type: methods.REQUESTED_SET_PAGE, currentWindow}
};

const getCard = (currentWindow) => {
    return {type: methods.REQUESTED_GET_CARD, currentWindow}
};

const resetPass = (currentWindow) => {
    return {type: methods.REQUESTED_RESET_PASS, currentWindow}
};


const BankomatActions = {

    resetPass(dispatch: Function, page){
        dispatch(resetPass(page));
    },


    sendMoney(dispatch: Function, data: Object) {

        dispatch(requestAsyncStart());

        const url = AbstractAction.url + 'transferFromTo';

        data.accountId = Number(data.accountId);
        data.accountNumberTo = Number(data.accountNumberTo);
        data.amount = Number(data.amount);

        return AbstractAction.ajaxToServer(
            url,
            data,
            data => dispatch(requestAsyncSuccess(data)),
            err => dispatch(requestAsyncFailure())
        );
    },

    getMoney(dispatch: Function, data: Object) {

        dispatch(requestAsyncStart());

        const url = AbstractAction.url + 'getMoneyFromAccount';

        data.accountId = Number(data.accountId);
        data.amount = Number(data.amount);

        return AbstractAction.ajaxToServer(
            url,
            data,
            data => dispatch(requestAsyncSuccess(data)),
            err => dispatch(requestAsyncFailure())
        );
    },

    getUserInfo(dispatch: Function, data: Object) {

        dispatch(requestAsyncStart());

        data.accountId = Number(data.accountId);

        const url = AbstractAction.url + 'accounts/' + data.accountId;

        return AbstractAction.ajaxGetToServer(
            url,
            data => dispatch(requestAsyncSuccess(data)),
            err => dispatch(requestAsyncFailure(err))
        );
    },

    getCard(dispatch: Function, page){
        dispatch(getCard(page));
    },

    selectPage(dispatch: Function, page) {
        dispatch(setPage(page))
    },

    setUser(dispatch: Function, userCorrect, userId, userPIN, accountNumber) {
        dispatch(setUser(userCorrect, userId, userPIN, accountNumber));
    },

    incCounter(dispatch: Function, userId, loginCounters) {

        if (loginCounters[userId]) {
            loginCounters[userId] = loginCounters[userId] + 1;
        } else {
            loginCounters[userId] = 1;
        }

        dispatch(setCounter(loginCounters));
    },

    login(dispatch: Function, data: Object) {

        dispatch(requestAsyncStart());

        dispatch(setUser(
            false,
            data.accountNumber,
            data.PIN,
            data.accountNumber,
        ));
        const url = AbstractAction.url + 'bankomatLogIn';

        data.accountNumber = Number(data.accountNumber);
        data.PIN = Number(data.PIN);

        return AbstractAction.ajaxToServer(
            url,
            data,
            data2 => dispatch(requestAsyncSuccess(data2)),
            err => dispatch(requestAsyncFailure(err))
        );
    },
};


export default BankomatActions;