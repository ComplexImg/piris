import {AbstractAction} from './abstractAction';
import {methods} from "../reducers/NewContractFormReducer";

const requestEventsError = () => {
    return {type: methods.REQUESTED_FAILED}
};
const requestDipositsSuccess = (data) => {
    return {type: methods.REQUESTED_SET_DEPOSITS, data: data}
};

const requestEventsSuccesed = () => {
    return {type: methods.REQUESTED_SUCCEEDED}
};

const requestAsyncStart = () => {
    return {type: methods.REQUESTED_START_ASYNC}
};
const requestAsyncSuccess = () => {
    return {type: methods.REQUESTED_SUCCESS_ASYNC}
};
const requestAsyncFailure = (err) => {
    return {type: methods.REQUESTED_FAILURE_ASYNC}
};


const NewContractFormActions = {

    allIsLoaded(dispatch: Function) {
        dispatch(requestEventsSuccesed())
    },

    loadDipositsLibrary(dispatch: Function) {
        const url = AbstractAction.url + 'deposits';

        return AbstractAction.ajaxGetToServer(
            url,
            data => dispatch(requestDipositsSuccess(data)),
            err => dispatch(requestEventsError())
        )
    },


    saveContract(dispatch: Function, data: Object) {

        requestAsyncStart();

        const url = AbstractAction.url + 'assignNewDeposit';

        data.amount = Number(data.amount);
        data.currencyId = Number(data.currencyId);
        data.depositId = Number(data.depositId);
        data.passportNumber = Number(data.passportNumber);

        console.log('saveContract', data);

        return AbstractAction.ajaxToServer(
            url,
            data,
            data => dispatch(requestAsyncSuccess()),
            err => dispatch(requestAsyncFailure())
        );
    },
};


export default NewContractFormActions;