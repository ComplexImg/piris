import {AbstractAction} from './abstractAction';
import { methods } from "../reducers/FormDataReducer";


const requestEventsError = () => {
    return {type: methods.REQUESTED_FAILED}
};
const requestCitiesSuccess = (data) => {
    return {type: methods.REQUESTED_SET_CITIES, data: data}
};
const requestCitizenshipsSuccess = (data) => {
    return {type: methods.REQUESTED_SET_CITIZENSHIPS, data: data}
};
const requestMaritalSuccess = (data) => {
    return {type: methods.REQUESTED_SET_MARITAL_STATUSES, data: data}
};
const requestDisabilitiesSuccess = (data) => {
    return {type: methods.REQUESTED_SET_DISABILITIES, data: data}
};
const requestEventsSuccesed = () => {
    return {type: methods.REQUESTED_SUCCEEDED}
};

const requestAsyncStart = () => {
    return {type: methods.REQUESTED_START_ASYNC}
};
const requestAsyncSuccess = () => {
    return {type: methods.REQUESTED_SUCCESS_ASYNC}
};
const requestAsyncFailure = (err) => {
    console.log('requestAsyncFailure', err);

    return {type: methods.REQUESTED_FAILURE_ASYNC}
};


const ClientFormActions = {

    allIsLoaded(dispatch: Function) {
        dispatch(requestEventsSuccesed())
    },

    loadCitiesLibrary(dispatch: Function) {
        const url = AbstractAction.url + 'cities';

        return AbstractAction.ajaxGetToServer(
            url,
            data => dispatch(requestCitiesSuccess(data)),
            err => dispatch(requestEventsError())
        )
    },


    loadMaritalLibrary(dispatch: Function) {
        const url = AbstractAction.url + 'maritalstatus';

        return AbstractAction.ajaxGetToServer(
            url,
            data => dispatch(requestMaritalSuccess(data)),
            err => dispatch(requestEventsError())
        )
    },


    loadCitizenshipsLibrary(dispatch: Function) {
        const url = AbstractAction.url + 'citizenships';

        return AbstractAction.ajaxGetToServer(
            url,
            data => dispatch(requestCitizenshipsSuccess(data)),
            err => dispatch(requestEventsError())
        )
    },


    loadDisabilitiesLibrary(dispatch: Function) {
        const url = AbstractAction.url + 'disabilities';

        return AbstractAction.ajaxGetToServer(
            url,
            data => dispatch(requestDisabilitiesSuccess(data)),
            err => dispatch(requestEventsError())
        )
    },


    saveClient(dispatch: Function, data: Object) {

        data.citizenshipId = 1;
        data.gender = 1;

        let url = AbstractAction.url + 'clients';

        dispatch(requestAsyncStart());

        if (data.id) {
            url += "/" + data.id;
            return AbstractAction.ajaxToServer(
                url,
                data,
                data => dispatch(requestAsyncSuccess()),
                err => dispatch(requestAsyncFailure()),
                "PUT"
            )
        } else {
            delete data.id;

            return AbstractAction.ajaxToServer(
                url,
                data,
                data => dispatch(requestAsyncSuccess()),
                err => dispatch(requestAsyncFailure())
            )
        }

    },

};


export default ClientFormActions;