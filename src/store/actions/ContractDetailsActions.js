import { AbstractAction } from './abstractAction';

const requestAsyncStart = () => {
    return {type: 'REQUESTED_START_ASYNC'}
};
const requestAsyncSuccess = (data) => {
    return {type: 'REQUESTED_SUCCESS_ASYNC', data: data}
};
const requestAsyncFailure = (err) => {
    return {type: 'REQUESTED_FAILURE_ASYNC'}
};


const ContractDetailsActions = {

    loadContractDetails(dispatch: Function, contractId) {

        requestAsyncStart();

        const url =  AbstractAction.url + 'accounts/' + contractId;

        return AbstractAction.ajaxGetToServer(
            url,
            data => dispatch(requestAsyncSuccess(data)),
            err => dispatch(requestAsyncFailure())
        )
    },

    closeDayDetails(dispatch: Function) {

        requestAsyncStart();

        const url =  AbstractAction.url + 'closeBankDay';

        return AbstractAction.ajaxGetToServer(
            url,
            data => dispatch(requestAsyncSuccess(data)),
            err => dispatch(requestAsyncFailure())
        )
    },


};


export default ContractDetailsActions;