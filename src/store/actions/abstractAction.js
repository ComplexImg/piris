// const serverUrl = 'https://localhost:44365/api/';

export const AbstractAction = {

    // url: 'https://localhost:44365/api/',
    url: 'http://piris/',

    ajaxGetToServer(url: string, callback_for_correct_result: Function, callback_for_error: Function) {
        fetch(url)
            .then(res => res.json())
            .then(
                result => callback_for_correct_result(result),
                error => callback_for_error(error)
            )
    },


    ajaxToServer(url: string, body_params, callback_for_correct_result: Function, callback_for_error: Function, method = "POST") {
        fetch(url, {
            method: method,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body_params)
        })
            .then(res => res.json())
            .then(
                result => console.log("result", result) || callback_for_correct_result(result),
                error => console.log("error", error) || callback_for_error(error)
            )
    },

};
