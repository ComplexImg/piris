import {AbstractAction} from './abstractAction';

import {methods} from "../reducers/TicketWindowReducer";

const requestAsyncStart = () => {
    return {type: methods.REQUESTED_START_ASYNC}
};
const requestAsyncSuccess = (data) => {
    return {type: methods.REQUESTED_SUCCESS_ASYNC, data: data}
};
const requestAsyncFailure = (err) => {
    return {type: methods.REQUESTED_FAILURE_ASYNC}
};


const TicketWindowActions = {

    newPay(dispatch: Function, data: Object) {

        data.accountNumber = Number(data.accountNumber);
        data.currencyTypeId = Number(data.currencyTypeId);
        data.amount = Number(data.amount);

        console.log('newPay', data);

        requestAsyncStart();

        const url = AbstractAction.url + 'AddToAccount';

        return AbstractAction.ajaxToServer(
            url,
            data,
            data => dispatch(requestAsyncSuccess(data)),
            err => dispatch(requestAsyncFailure())
        )
    },

};


export default TicketWindowActions;