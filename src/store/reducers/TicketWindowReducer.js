const initialState = {
    asyncOperationInProgress: false,
    asyncResult: null,
    details: null
};

const ticketWindowReducer = (state = initialState, action) => {
    switch (action.type) {
        case methods.REQUESTED_START_ASYNC:
            return {
                details: state.details,
                asyncOperationInProgress: true,
                asyncResult: null,
            };
        case methods.REQUESTED_SUCCESS_ASYNC:
            return {
                details: action.data,
                asyncOperationInProgress: false,
                asyncResult: true,
            };
        case methods.REQUESTED_FAILURE_ASYNC:
            return {
                details: state.details,
                asyncOperationInProgress: false,
                asyncResult: false,
            };
        default:
            return state;
    }
};

export default ticketWindowReducer;

export const methods = {
    REQUESTED_START_ASYNC: 'TWR_REQUESTED_START_ASYNC',
    REQUESTED_SUCCESS_ASYNC: 'TWR_REQUESTED_SUCCESS_ASYNC',
    REQUESTED_FAILURE_ASYNC: 'TWR_REQUESTED_FAILURE_ASYNC',
};