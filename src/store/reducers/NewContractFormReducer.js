const initialState = {
    loaded: false,
    error: false,
    asyncOperationInProgress: false,
    asyncResult: null,

    deposits: [],
};

const newContractFormReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'NCFR_REQUESTED_START_ASYNC':
            return {
                loaded: state.loaded,
                error: state.error,

                deposits: state.deposits,

                asyncOperationInProgress: true,
                asyncResult: null,
            };
        case 'NCFR_REQUESTED_SUCCESS_ASYNC':
            return {
                loaded: state.loaded,
                error: state.error,

                deposits: state.deposits,


                asyncOperationInProgress: false,
                asyncResult: true,
            };
        case 'NCFR_REQUESTED_FAILURE_ASYNC':
            return {
                loaded: state.loaded,
                error: state.error,

                deposits: state.deposits,

                asyncOperationInProgress: false,
                asyncResult: false,
            };
        case 'NCFR_REQUESTED_SUCCEEDED':
            return {
                loaded: true,
                error: false,

                deposits: state.deposits,

                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };
        case 'NCFR_REQUESTED_FAILED':
            return {
                loaded: false,
                error: true,

                deposits: state.deposits,

                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };
        case 'NCFR_REQUESTED_SET_DEPOSITS':
            return {
                loaded: state.loaded,
                error: state.error,

                deposits:  action.data,

                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };
        default:
            return state;
    }
};

export default newContractFormReducer;

export const methods = {
    REQUESTED_START_ASYNC : 'NCFR_REQUESTED_START_ASYNC',
    REQUESTED_SUCCESS_ASYNC : 'NCFR_REQUESTED_SUCCESS_ASYNC',
    REQUESTED_FAILURE_ASYNC : 'NCFR_REQUESTED_FAILURE_ASYNC',
    REQUESTED_SUCCEEDED : 'NCFR_REQUESTED_SUCCEEDED',
    REQUESTED_FAILED : 'NCFR_REQUESTED_FAILED',
    REQUESTED_SET_DEPOSITS : 'NCFR_REQUESTED_SET_DEPOSITS',
};