const initialState = {
    loaded: false,
    error: false,
    asyncOperationInProgress: false,
    asyncResult: null,

    cities: [],
    marital_statuses: [],
    citizenships: [],
    disabilities: [],
};

const formDataReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'FDR_REQUESTED_START_ASYNC':
            return {
                loaded: state.loaded,
                error: state.error,
                cities: state.cities,
                marital_statuses: state.marital_statuses,
                citizenships: state.citizenships,
                disabilities: state.disabilities,
                asyncOperationInProgress: true,
                asyncResult: null,
            };
        case 'FDR_REQUESTED_SUCCESS_ASYNC':
            return {
                loaded: state.loaded,
                error: state.error,
                cities: state.cities,
                marital_statuses: state.marital_statuses,
                citizenships: state.citizenships,
                disabilities: state.disabilities,
                asyncOperationInProgress: false,
                asyncResult: true,
            };

        case 'FDR_REQUESTED_FAILURE_ASYNC':
            return {
                loaded: state.loaded,
                error: state.error,
                cities: state.cities,
                marital_statuses: state.marital_statuses,
                citizenships: state.citizenships,
                disabilities: state.disabilities,
                asyncOperationInProgress: false,
                asyncResult: false,
            };

        case 'FDR_REQUESTED_SUCCEEDED':
            return {
                loaded: true,
                error: false,
                cities: state.cities,
                marital_statuses: state.marital_statuses,
                citizenships: state.citizenships,
                disabilities: state.disabilities,
                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };
        case 'FDR_REQUESTED_FAILED':
            return {
                loaded: false,
                error: true,
                cities: state.cities,
                marital_statuses: state.marital_statuses,
                citizenships: state.citizenships,
                disabilities: state.disabilities,
                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };
        case 'FDR_REQUESTED_SET_CITIES':
            return {
                loaded: state.loaded,
                error: state.error,
                cities: action.data,
                marital_statuses: state.marital_statuses,
                citizenships: state.citizenships,
                disabilities: state.disabilities,
                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };

        case 'FDR_REQUESTED_SET_MARITAL_STATUSES':
            return {
                loaded: state.loaded,
                error: state.error,
                cities: state.cities,
                marital_statuses: action.data,
                citizenships: state.citizenships,
                disabilities: state.disabilities,
                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };

        case 'FDR_REQUESTED_SET_CITIZENSHIPS':
            return {
                loaded: state.loaded,
                error: state.error,
                cities: state.cities,
                marital_statuses: state.marital_statuses,
                citizenships: action.data,
                disabilities: state.disabilities,
                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };

        case 'FDR_REQUESTED_SET_DISABILITIES':
            return {
                loaded: state.loaded,
                error: state.error,
                cities: state.cities,
                marital_statuses: state.marital_statuses,
                citizenships: state.citizenships,
                disabilities: action.data,
                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };
        default:
            return state;
    }
};

export default formDataReducer;

export const methods = {
    REQUESTED_SET_DISABILITIES: 'FDR_REQUESTED_SET_DISABILITIES',
    REQUESTED_SET_CITIZENSHIPS: 'FDR_REQUESTED_SET_CITIZENSHIPS',
    REQUESTED_SET_MARITAL_STATUSES: 'FDR_REQUESTED_SET_MARITAL_STATUSES',
    REQUESTED_SET_CITIES: 'FDR_REQUESTED_SET_CITIES',
    REQUESTED_FAILED: 'FDR_REQUESTED_FAILED',
    REQUESTED_SUCCEEDED: 'FDR_REQUESTED_SUCCEEDED',
    REQUESTED_FAILURE_ASYNC: 'FDR_REQUESTED_FAILURE_ASYNC',
    REQUESTED_SUCCESS_ASYNC: 'FDR_REQUESTED_SUCCESS_ASYNC',
    REQUESTED_START_ASYNC: 'FDR_REQUESTED_START_ASYNC',

};