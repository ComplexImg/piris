const initialState = {
    clients: [],
    error: false,
    loaded: false,
};

const ClientsTableReducer = (state = initialState, action) => {
    switch (action.type) {

        case 'CTR_REQUESTED_CLIENTS_ASYNC':
            return {
                clients: state.clients,
                error: false,
                loaded: false,
            };

        case 'CTR_REQUESTED_CLIENTS_ASYNC_FAILED':
            return {
                clients: [],
                error: true,
                loaded: false,
            };
        case 'CTR_REQUESTED_CLIENTS_SUCCESS':
            return {
                clients: action.data,
                error: false,
                loaded: true,
            };
        case 'CTR_REQUESTED_CLIENTS_FAILURE':
            return {
                clients: [],
                error: true,
                loaded: false,
            };
        default:
            return state;
    }
};

export default ClientsTableReducer;

export const methods = {
    REQUESTED_CLIENTS_ASYNC: 'CTR_REQUESTED_CLIENTS_ASYNC',
    REQUESTED_CLIENTS_ASYNC_FAILED: 'CTR_REQUESTED_CLIENTS_ASYNC_FAILED',
    REQUESTED_CLIENTS_SUCCESS: 'CTR_REQUESTED_CLIENTS_SUCCESS',
    REQUESTED_CLIENTS_FAILURE: 'CTR_REQUESTED_CLIENTS_FAILURE',
};