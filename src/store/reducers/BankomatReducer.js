const initialState = {
    userCorrect: false,
    accountNumber: null,
    userId: null,
    userPIN: null,
    loginCounters: {},

    currentWindow: null,
    response: null,

    asyncOperationInProgress: false,
    asyncResult: null,
};


export const methods = {
    REQUESTED_RESET_PASS: 'BA_REQUESTED_RESET_PASS',
    REQUESTED_GET_CARD: 'BA_REQUESTED_GET_CARD',
    REQUESTED_SET_PAGE: 'BA_REQUESTED_SET_PAGE',
    REQUESTED_SET_USER: 'BA_REQUESTED_SET_USER',
    REQUESTED_SET_COUNTER: 'BA_REQUESTED_SET_COUNTER',
    REQUESTED_START_ASYNC: 'BA_REQUESTED_START_ASYNC',
    REQUESTED_SUCCESS_ASYNC: 'BA_REQUESTED_SUCCESS_ASYNC',
    REQUESTED_FAILURE_ASYNC: 'BA_REQUESTED_FAILURE_ASYNC',
    REQUESTED_SUCCEEDED: 'BA_REQUESTED_SUCCEEDED',
    REQUESTED_FAILED: 'BA_REQUESTED_FAILED',
    REQUESTED_SET_CREDIT: 'BA_REQUESTED_SET_CREDIT',
};


const bankomatReducer = (state = initialState, action) => {
    console.log(action.type);
    switch (action.type) {
        case methods.REQUESTED_RESET_PASS:
            return {
                userCorrect: false,
                userId: null,
                accountNumber: state.accountNumber,
                userPIN: null,
                loginCounters: state.loginCounters,

                currentWindow: action.currentWindow,
                response: state.response,

                asyncOperationInProgress: false,
                asyncResult: null,
            };

        case methods.REQUESTED_GET_CARD:
            return {
                userCorrect: false,
                userId: null,
                userPIN: null,
                accountNumber: null,
                loginCounters: state.loginCounters,

                currentWindow: action.currentWindow,
                response: state.response,

                asyncOperationInProgress: false,
                asyncResult: null,
            };
        case methods.REQUESTED_SET_PAGE:
            return {
                userCorrect: state.userCorrect,
                userId: state.userId,
                userPIN: state.userPIN,
                accountNumber: state.accountNumber,
                loginCounters: state.loginCounters,

                currentWindow: action.currentWindow,
                response: null,

                asyncOperationInProgress: false,
                asyncResult: null,
            };
        case methods.REQUESTED_SET_COUNTER:
            return {
                userCorrect: state.userCorrect,
                userId: state.userId,
                accountNumber: state.accountNumber,
                userPIN: state.userPIN,
                loginCounters: action.loginCounters,

                currentWindow: state.currentWindow,
                response: state.response,

                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };
        case methods.REQUESTED_SET_USER:
            return {
                userCorrect: action.userCorrect,
                userId: action.userId,
                userPIN: action.userPIN,
                accountNumber: action.accountNumber,
                loginCounters: state.loginCounters,

                currentWindow: state.currentWindow,
                response: state.response,

                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };
        case methods.REQUESTED_START_ASYNC:
            return {
                userCorrect: state.userCorrect,
                userId: state.userId,
                userPIN: state.userPIN,
                accountNumber: state.accountNumber,
                loginCounters: state.loginCounters,

                currentWindow: state.currentWindow,
                response: state.response,

                asyncOperationInProgress: true,
                asyncResult: null,
            };
        case methods.REQUESTED_SUCCESS_ASYNC:
            return {
                userCorrect: state.userCorrect,
                userId: state.userId,
                userPIN: state.userPIN,
                accountNumber: state.accountNumber,
                loginCounters: state.loginCounters,

                currentWindow: state.currentWindow,
                response: action.data,

                asyncOperationInProgress: false,
                asyncResult: true,
            };
        case methods.REQUESTED_FAILURE_ASYNC:
            return {
                userCorrect: state.userCorrect,
                userId: state.userId,
                userPIN: state.userPIN,
                accountNumber: state.accountNumber,
                loginCounters: state.loginCounters,

                currentWindow: state.currentWindow,
                response: state.response,

                asyncOperationInProgress: false,
                asyncResult: false,
            };
        default:
            return state;
    }
};

export default bankomatReducer;
