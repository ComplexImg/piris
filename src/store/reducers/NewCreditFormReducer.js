const initialState = {
    loaded: false,
    error: false,
    asyncOperationInProgress: false,
    asyncResult: null,

    credits: [],
};

const newCreditFormReducer = (state = initialState, action) => {
    switch (action.type) {
        case methods.REQUESTED_START_ASYNC:
            return {
                loaded: state.loaded,
                error: state.error,

                credits: state.credits,

                asyncOperationInProgress: true,
                asyncResult: null,
            };
        case methods.REQUESTED_SUCCESS_ASYNC:
            return {
                loaded: state.loaded,
                error: state.error,

                credits: state.credits,


                asyncOperationInProgress: false,
                asyncResult: true,
            };
        case methods.REQUESTED_FAILURE_ASYNC:
            return {
                loaded: state.loaded,
                error: state.error,

                credits: state.credits,

                asyncOperationInProgress: false,
                asyncResult: false,
            };
        case methods.REQUESTED_SUCCEEDED:
            return {
                loaded: true,
                error: false,

                credits: state.credits,

                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };
        case methods.REQUESTED_FAILED :
            return {
                loaded: false,
                error: true,

                credits: state.credits,

                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };
        case methods.REQUESTED_SET_CREDIT:
            return {
                loaded: state.loaded,
                error: state.error,

                credits: action.data,

                asyncOperationInProgress: state.asyncOperationInProgress,
                asyncResult: state.asyncResult,
            };
        default:
            return state;
    }
};

export default newCreditFormReducer;

export const methods = {
    REQUESTED_START_ASYNC: 'NCrFR_REQUESTED_START_ASYNC',
    REQUESTED_SUCCESS_ASYNC: 'NCrFR_REQUESTED_SUCCESS_ASYNC',
    REQUESTED_FAILURE_ASYNC: 'NCrFR_REQUESTED_FAILURE_ASYNC',
    REQUESTED_SUCCEEDED: 'NCrFR_REQUESTED_SUCCEEDED',
    REQUESTED_FAILED: 'NCrFR_REQUESTED_FAILED',
    REQUESTED_SET_CREDIT: 'NCrFR_REQUESTED_SET_CREDIT',
};