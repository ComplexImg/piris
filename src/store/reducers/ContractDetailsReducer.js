const initialState = {
    asyncOperationInProgress: false,
    asyncResult: null,
    details: null
};

const contractDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUESTED_START_ASYNC':
            return {
                details: state.details,
                asyncOperationInProgress: true,
                asyncResult: null,
            };
        case 'REQUESTED_SUCCESS_ASYNC':
            return {
                details: action.data,
                asyncOperationInProgress: false,
                asyncResult: true,
            };
        case 'REQUESTED_FAILURE_ASYNC':
            return {
                details: state.details,
                asyncOperationInProgress: false,
                asyncResult: false,
            };
        default:
            return state;
    }
};

export default contractDetailsReducer;