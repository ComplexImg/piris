import {createStore, combineReducers} from 'redux';

import formDataReducer from './reducers/FormDataReducer'
import ClientsTableReducer from './reducers/ClientsTableReducer'
import newContractFormReducer from './reducers/NewContractFormReducer'
import contractDetailsReducer from './reducers/ContractDetailsReducer'
import ticketWindowReducer from './reducers/TicketWindowReducer'
import newCreditFormReducer from "./reducers/NewCreditFormReducer";
import bankomatReducer from "./reducers/BankomatReducer";


const reducers = combineReducers({
    formDataReducer,
    ClientsTableReducer,
    newContractFormReducer,
    contractDetailsReducer,
    ticketWindowReducer,
    newCreditFormReducer,
    bankomatReducer
});

const Store = createStore(reducers);

export default Store