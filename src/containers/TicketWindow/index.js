import React, {Component} from 'react';
import TicketWindowForm from "../../components/TicketWindowForm";
import Store from '../../store/Store';
import { Provider } from 'react-redux';

class TicketWindow extends Component {

    constructor(props) {
        super(props);
        this.render = this.render.bind(this);
    }


    render() {
        return (
            <div>
                <Provider store={Store}><TicketWindowForm/></Provider>
            </div>
        );
    }
}

export default TicketWindow;