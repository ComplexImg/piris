import React, {Component} from 'react';
import CloseBankDayForm from "../../components/CloseBankDayForm";
import Store from '../../store/Store';
import { Provider } from 'react-redux';

class CloseBankDay extends Component {

    constructor(props) {
        super(props);
        this.render = this.render.bind(this);
    }


    render() {
        return (
            <div>
                <Provider store={Store}><CloseBankDayForm /></Provider>
            </div>
        );
    }
}

export default CloseBankDay;