import React, {Component} from 'react';
import NewCreditForm from "../../components/NewCreditForm";
import Store from '../../store/Store';
import { Provider } from 'react-redux';

class NewCredit extends Component {

    constructor(props) {
        super(props);
        this.render = this.render.bind(this);
    }


    render() {
        return (
            <div>
                <Provider store={Store}><NewCreditForm/></Provider>
            </div>
        );
    }
}

export default NewCredit;