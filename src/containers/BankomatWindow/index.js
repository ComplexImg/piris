import React, {Component} from 'react';
import BankomatMain from "../../components/BankomatComponents/BankomatMain";
import Store from '../../store/Store';
import { Provider } from 'react-redux';

class BankomatWindow extends Component {

    constructor(props) {
        super(props);
        this.render = this.render.bind(this);
    }


    render() {
        return (
            <div>
                <Provider store={Store}><BankomatMain /></Provider>
            </div>
        );
    }
}

export default BankomatWindow;