import React, {Component} from 'react';
import NewContractForm from "../../components/NewContractForm";
import Store from '../../store/Store';
import { Provider } from 'react-redux';

class NewContract extends Component {

    constructor(props) {
        super(props);
        this.render = this.render.bind(this);
    }


    render() {
        return (
            <div>
                <Provider store={Store}><NewContractForm/></Provider>
            </div>
        );
    }
}

export default NewContract;