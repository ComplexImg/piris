import React, {Component} from 'react';
import EditClientForm from "../../components/EditClientForm";
import Store from '../../store/Store';
import { Provider } from 'react-redux';

class Init extends Component {

    constructor(props) {
        super(props);
        this.render = this.render.bind(this);
    }


    render() {
        return (
            <div>
                <Provider store={Store}><EditClientForm/></Provider>
            </div>
        );
    }
}

export default Init;