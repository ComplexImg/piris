import React, {Component} from 'react';
import ContractDetailsForm from "../../components/ContractDetailsForm";
import Store from '../../store/Store';
import { Provider } from 'react-redux';

class ContractDetails extends Component {

    constructor(props) {
        super(props);
        this.render = this.render.bind(this);
    }


    render() {
        return (
            <div>
                <Provider store={Store}><ContractDetailsForm/></Provider>
            </div>
        );
    }
}

export default ContractDetails;