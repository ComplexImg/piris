import React, {Component} from 'react';
import Store from "../../store/Store";
import {Provider} from 'react-redux';
import ClientsOverviewList from '../../components/ClientsOverviewList';

class ClientsTable extends Component {
    render() {
        return (
            <div>
                <Provider store={Store}><ClientsOverviewList/></Provider>
            </div>
        );
    }
}


export default ClientsTable;


