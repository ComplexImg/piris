<?php

function logData($data)
{
    $date = date("c");
    $str = " url: {$_SERVER['REQUEST_URI']}\n method: {$_SERVER['REQUEST_METHOD']}\n $date \n" . json_encode($data) . "\n\n";
    file_put_contents('log.txt', $str, FILE_APPEND | LOCK_EX);
}


$routes = explode('/', $_SERVER['REQUEST_URI']);


header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');


$data = json_decode(file_get_contents('php://input'), true);

//logData($data);


if ($_SERVER['REQUEST_METHOD'] === 'POST') {


    if ($routes[1] === 'clients') {

        echo 'ddd';

    }


} else {

    if ($routes[1] === 'bankomatLogIn') {
        echo json_encode(array(
            'accountId' => -1,
//            'accountId' => '-1',
        ));
    }


    if ($routes[1] === 'getMoneyFromAccount') {
        echo json_encode(array(
            "isSuccess" => true,
            "details" => 'sdasd'
        ));
    }

    if ($routes[1] === 'send_money') {
        echo json_encode(array(
            "isSuccess" => true,
            "Details" => 'Ti dolboeb((('
        ));
    }

    if ($routes[1] === 'get_user_info') {
        echo json_encode(array(
            'accountId' => 34563456,
            'dsfgd' => 'sdfgsdfgsdfg',
            'sdhfsdfhsdfh' => '234sdfxcvbrth ertge rth',
//            'accountId' => '-1',
        ));
    }

    if ($routes[1] === 'maritalstatus') {
        echo json_encode(array(
            array(
                'id' => 123,
                'name' => 'Женат'
            ),
            array(
                'id' => 516,
                'name' => 'Не женат'
            ),
            array(
                'id' => 7643,
                'name' => 'ыхыхыхы'
            ),
        ));
    }

    if ($routes[1] === 'citizenships') {
        echo json_encode(array(
            array(
                'id' => 12,
                'name' => 'РБ'
            ),
            array(
                'id' => 56,
                'name' => 'РФ'
            ),
            array(
                'id' => 763,
                'name' => 'КНДР'
            ),
        ));
    }

    if ($routes[1] === 'disabilities') {
        echo json_encode(array(
            array(
                'id' => 2,
                'name' => 'Рак'
            ),
            array(
                'id' => 6,
                'name' => 'Танкист'
            ),
            array(
                'id' => 73,
                'name' => 'аааа'
            ),
        ));
    }

    if ($routes[1] === 'cities') {
        echo json_encode(array(
            array(
                'id' => 2,
                'name' => 'Minsk'
            ),
            array(
                'id' => 4,
                'name' => 'Moskow'
            ),
            array(
                'id' => 7,
                'name' => 'New York'
            ),
            array(
                'id' => 8,
                'name' => 'Gomel'
            ),
            array(
                'id' => 45,
                'name' => 'Berlin'
            ),
            array(
                'id' => 223,
                'name' => 'Vitebsk'
            ),
        ));
    }

    if ($routes[1] === 'clients') {
        echo json_encode(array(
            array(
                'id' => 2,
                'middleName' => 'Sidorenko',
                'firstName' => 'Slava',
                'lastName' => 'Sergeevich',
                'birthDate' => '2018-10-01',
                'passportSeries' => 'MP',
                'passportNumber' => '123',
            ),
            array(
                'id' => 5,
                'middleName' => 'Sidorenko4',
                'firstName' => 'Slava4',
                'lastName' => 'Sergeevic4h',
                'birthDate' => '2018-10-01',
                'passportSeries' => 'MP',
                'passportNumber' => '1244443',
            ),
        ));
    }


    if ($routes[1] === 'deposits') {

        echo json_encode(


            [
                [
                    "id" => 1,
                    "type" => 0,
                    "name" => "Срочный отзывный",
                    "description" => null,
                    "minExpirationTerm" => 7,
                    "maxExpirationTerm" => 1500,
                    "currencyTypes" =>
                        [
                            [
                                "id" => 1,
                                "depositId" => 1,
                                "currencyTypeId" => 1,
                                "currencyType" =>
                                    [
                                        "id" => 1,
                                        "name" => "BYN"
                                    ],
                                "value" => 6.900000000
                            ],
                            [
                                "id" => 2,
                                "depositId" => 1,
                                "currencyTypeId" => 2,
                                "currencyType" =>
                                    [
                                        "id" => 2,
                                        "name" => "RUB"
                                    ],
                                "value" => 3.500000000
                            ],
                            [
                                "id" => 3,
                                "depositId" => 1,
                                "currencyTypeId" => 3,
                                "currencyType" =>
                                    [
                                        "id" => 3,
                                        "name" => "USD"
                                    ],
                                "value" => 1.200000000
                            ],
                            [
                                "id" => 4,
                                "depositId" => 1,
                                "currencyTypeId" => 4,
                                "currencyType" =>
                                    [
                                        "id" => 4,
                                        "name" => "EUR"
                                    ],
                                "value" => 0.500000000
                            ]
                        ]
                ],
                [
                    "id" => 2,
                    "type" => 1,
                    "name" => "Срочный безотзывный",
                    "description" => null,
                    "minExpirationTerm" => 30,
                    "maxExpirationTerm" => 3000,
                    "currencyTypes" =>
                        [
                            [
                                "id" => 5,
                                "depositId" => 2,
                                "currencyTypeId" => 1,
                                "currencyType" =>
                                    [
                                        "id" => 1,
                                        "name" => "BYN"
                                    ],
                                "value" => 8.000000000
                            ],
                            [
                                "id" => 6,
                                "depositId" => 2,
                                "currencyTypeId" => 2,
                                "currencyType" =>
                                    [
                                        "id" => 2,
                                        "name" => "RUB"
                                    ],
                                "value" => 3.750000000
                            ],
                            [
                                "id" => 7,
                                "depositId" => 2,
                                "currencyTypeId" => 3,
                                "currencyType" =>
                                    [
                                        "id" => 3,
                                        "name" => "USD"
                                    ],
                                "value" => 1.500000000
                            ],
                            [
                                "id" => 8,
                                "depositId" => 2,
                                "currencyTypeId" => 4,
                                "currencyType" =>
                                    [
                                        "id" => 4,
                                        "name" => "EUR"
                                    ],
                                "value" => 0.700000000
                            ]
                        ]
                ]
            ]
        );

    }


    if ($routes[1] === 'credits') {

        echo json_encode(


            [
                [
                    "id" => 1,
                    "type" => 0,
                    "name" => "кредииит отзывный",
                    "description" => null,
                    "minExpirationTerm" => 7,
                    "maxExpirationTerm" => 1500,
                    "currencyTypes" =>
                        [
                            [
                                "id" => 1,
                                "creditId" => 1,
                                "currencyTypeId" => 1,
                                "currencyType" =>
                                    [
                                        "id" => 1,
                                        "name" => "BYN"
                                    ],
                                "value" => 6.900000000
                            ],
                            [
                                "id" => 2,
                                "creditId" => 1,
                                "currencyTypeId" => 2,
                                "currencyType" =>
                                    [
                                        "id" => 2,
                                        "name" => "RUB"
                                    ],
                                "value" => 3.500000000
                            ],
                            [
                                "id" => 3,
                                "creditId" => 1,
                                "currencyTypeId" => 3,
                                "currencyType" =>
                                    [
                                        "id" => 3,
                                        "name" => "USD"
                                    ],
                                "value" => 1.200000000
                            ],
                            [
                                "id" => 4,
                                "creditId" => 1,
                                "currencyTypeId" => 4,
                                "currencyType" =>
                                    [
                                        "id" => 4,
                                        "name" => "EUR"
                                    ],
                                "value" => 0.500000000
                            ]
                        ]
                ],
                [
                    "id" => 2,
                    "type" => 1,
                    "name" => "Срочный rhtrlbn",
                    "description" => null,
                    "minExpirationTerm" => 30,
                    "maxExpirationTerm" => 3000,
                    "currencyTypes" =>
                        [
                            [
                                "id" => 5,
                                "creditId" => 2,
                                "currencyTypeId" => 1,
                                "currencyType" =>
                                    [
                                        "id" => 1,
                                        "name" => "BYN"
                                    ],
                                "value" => 8.000000000
                            ],
                            [
                                "id" => 6,
                                "creditId" => 2,
                                "currencyTypeId" => 2,
                                "currencyType" =>
                                    [
                                        "id" => 2,
                                        "name" => "RUB"
                                    ],
                                "value" => 3.750000000
                            ],
                            [
                                "id" => 7,
                                "creditId" => 2,
                                "currencyTypeId" => 3,
                                "currencyType" =>
                                    [
                                        "id" => 3,
                                        "name" => "USD"
                                    ],
                                "value" => 1.500000000
                            ],
                            [
                                "id" => 8,
                                "creditId" => 2,
                                "currencyTypeId" => 4,
                                "currencyType" =>
                                    [
                                        "id" => 4,
                                        "name" => "EUR"
                                    ],
                                "value" => 0.700000000
                            ]
                        ]
                ]
            ]
        );

    }


    if ($routes[1] === 'contract' && $routes[2] === '123') {

        echo json_encode([
            "id" => 2,
            "type" => 1,
            "name" => "Срочный безотзывный",
            "description" => null,
            "minExpirationTerm" => 30,
        ]);

    }

    if ($routes[1] === 'contract' && $routes[2] === '456') {

        echo json_encode(["id" => 28, "accountNumber" => "1336000000061", "creationDate" => "2018-11-21", "currencyTypeId" => 2, "debit" => 0.000000000, "credit" => 1234.000000000, "saldo" => 0.000000000, "isClosed" => false, "accountName" => "1336000000061", "contractId" => 12, "clientId" => 9]);

    }


    if ($routes[1] === 'close') {

        echo json_encode([
            'close' => true
        ]);

    }

}
        
        
